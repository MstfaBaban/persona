<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'user_id', 'body'
    ];
    
    public function to_user()
    {
        return $this->belongsTo('App\User','to_user_id');
    }
    public function from_user()
    {
        return $this->belongsTo('App\User','from_user_id');
    }

}
