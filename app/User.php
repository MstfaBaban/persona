<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class User extends Authenticatable
{
    use Notifiable;
    use AuthenticableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */ 
    protected $fillable = [
        'name', 'email', 'password','ip','description','url_img'
    ];

    /** 
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getTraitExistsAttribute(){
        return count($this->follows_from_user);
     }

    public function comments_to_user(){
        return $this->hasMany('App\Comment','to_user_id');
    }
    public function comments_from_user(){
        return $this->hasMany('App\Comment','from_user_id');
    }

    public function traits_to_user(){
        return $this->hasMany('App\UserTrait','to_user_id');
    }
    public function traits_from_user(){
        return $this->hasMany('App\UserTrait','from_user_id');
    }

    public function follows_to_user(){
        return $this->hasMany('App\Follows','to_user_id');
    }
    public function follows_from_user(){
        return $this->hasMany('App\Follows','from_user_id');
    }

    public function follow_check(){

        $check = $this->where('from_user_id',auth()->id())->get();

        return $check;
        
    }


    public function getNameAttribute($value) {
        return ucfirst($value);
    }

    public function getRandomCommenterNameAttribute(){
        $fromUserId = $this->random_comment->from_user_id;
        $fromName = User::where('id',$fromUserId)->get()->first();
        return $fromName->name;
   }

    public function getRandomCommentAttribute(){
        return $this->comments_to_user()->inRandomOrder()->first();
   }

   public function getHasCommentsAttribute(){
       if(count($this->comments_to_user) > 0){
        return true;
       }
       else{
        return false;
       }
}

}

