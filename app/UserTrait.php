<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTrait extends Model
{
    
    protected $fillable = [
        'trait','to_user_id','from_user_id','button_id','catagory'
    ];


    public function to_user()
    {
        return $this->belongsTo('App\User','to_user_id');
    }

    public function from_user()
    {
        return $this->belongsTo('App\User','from_user_id');
    }


    
    
}
