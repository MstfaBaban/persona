<?php

namespace App\Http\Controllers;

use App\Follows;
use Illuminate\Http\Request;

class FollowsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


   


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
    
        $check = count(Follows::where('from_user_id',auth()->id())->where('to_user_id',$id)->get());
        

        if($check == 0){
            Follows::create([
                'follows'         => 1,
                'from_user_id'  => auth()->id(),
                'to_user_id'    => $id,
            ]);

            $followers_count = count(Follows::where('to_user_id',$id)->get());
            $following_count = count(Follows::where('from_user_id',$id)->get());

            $response = array(
                'status' => 'success',
                'state'  => 'Unfollow',
                'followers'  => $followers_count,
                'following'  => $following_count
            );

            return response()->json($response); 
            }
            
            else{
                $destroyCheck = Follows::where('from_user_id',auth()->id())->where('to_user_id',$id)->first();
                Follows::destroy($destroyCheck->id);
    
                $followers_count = count(Follows::where('to_user_id',$id)->get());
                $following_count = count(Follows::where('from_user_id',$id)->get());

                $response = array(
                    'status' => 'success',
                    'state'  => 'Follow',
                    'followers'  => $followers_count,
                    'following'  => $following_count
                );

                return response()->json($response);
            }
    }

    

    /**
     * Display the specified resource.
     *
     * @param  \App\Follows  $follows
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $check = count(Follows::where('from_user_id',auth()->id())->where('to_user_id',$id)->get());
        $followers_count = count(Follows::where('to_user_id',$id)->get());
        $following_count = count(Follows::where('from_user_id',$id)->get());
        

        if($check == 0){

            $response = array(
                'status' => 'success',
                'state'  => 'Follow',
                'followers'  => $followers_count,
                'following'  => $following_count
            );

            return response()->json($response); 
            }
            
            else{
                $response = array(
                    'status' => 'success',
                    'state'  => 'Unfollow',
                    'followers'  => $followers_count,
                    'following'  => $following_count
                );

                return response()->json($response);
            }
            dd("something went wrong");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Follows  $follows
     * @return \Illuminate\Http\Response
     */
    public function edit(Follows $follows)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Follows  $follows
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Follows $follows)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Follows  $follows
     * @return \Illuminate\Http\Response
     */
    public function destroy(Follows $follows)
    {
        //
    }
}
