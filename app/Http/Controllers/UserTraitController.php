<?php

namespace App\Http\Controllers;

use App\UserTrait;
use Illuminate\Http\Request;

class UserTraitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function traitNumCounter($singleTrait)
    {
        $counter=0;
        foreach ($singleTrait as $record) {
            
            switch($record->trait){

                case "very_bad": $counter=$counter-2;
                break;
                case "bad": $counter=$counter-1;
                break;
                case "neutral":   ;
                break;
                case "good": $counter=$counter+1;
                break;
                case "very_good": $counter=$counter+2;
                break;

            }
        }
        return $counter;
    }
    
    public function percentCalc($traits)
    {
        $open = $traits->where('catagory','open')->all();
        $open_count = count($open);
        $extra = $traits->where('catagory','extra')->all();
        $extra_count = count($extra);
        $cons = $traits->where('catagory','cons')->all();
        $cons_count = count($cons);
        $agre = $traits->where('catagory','agre')->all();
        $agre_count = count($agre);
        $nero = $traits->where('catagory','nero')->all();
        $nero_count = count($nero);

        $allNumsArray = array(
            'open' => round($this->toPercent($this->traitNumCounter($open),$open_count)),
            'extra' => round($this->toPercent($this->traitNumCounter($extra),$extra_count)),
            'cons' => round($this->toPercent($this->traitNumCounter($cons),$cons_count)),
            'agre' => round($this->toPercent($this->traitNumCounter($agre),$agre_count)),
            'nero'  => round($this->toPercent($this->traitNumCounter($nero),$nero_count)),
        );

        
        return $allNumsArray;
    }

    public function toPercent($traitValue, $traitcount)
    {
        
        $traitcount=$traitcount+5;

        if($traitcount==0){
            return 0;
        }

        else{
        $endPercentage = 50 + 25 * ($traitValue/$traitcount);
        return $endPercentage;
        }

    }

    public function store(Request $request,$id)
    { 
        
        $check = count(UserTrait::where('from_user_id',auth()->id())->where('to_user_id',$id)->where('catagory',$request->catagory)->get());
        

        
        if($check > 0){
        $destroyCheck = UserTrait::where('from_user_id',auth()->id())->where('to_user_id',$id)->where('catagory',$request->catagory)->first(); 
        UserTrait::destroy($destroyCheck->id);
        }
        UserTrait::create([
            'trait'         => $request->trait,
            'from_user_id'  => auth()->id(),
            'to_user_id'    => $id,
            'button_id'    => $request->button_id ,
            'catagory'    => $request->catagory
        ]);

        $numToPercent = UserTrait::where('to_user_id',$id)->get();
  
        $progressPercent = $this->percentCalc($numToPercent);

        $response = array(
            'status' => 'success',
            'active'  => 'active',
            'trait'  => $request->trait,
            'button_id'    => $request->button_id,
            'catagory'    => $request->catagory,
            'percent_array' => $progressPercent
        );
    
        return response()->json($response); 
        
    }

    public function check($id)
    {
        dd("This peice of code isnt working ATM");
        $check = count(UserTrait::where('from_user_id',auth()->id())->where('to_user_id',$id)->get());

        if($check == 0){
        $response = array(
            'status' => 'success',
            'color'  => 'blue'
        );
    
        return response()->json($response);
        }
        
        else{
            $response = array(
                'status' => 'success',
                'color'  => 'red'
            );

            return response()->json($response); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserTrait  $userTrait
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $check = count(UserTrait::where('from_user_id',auth()->id())->where('to_user_id',$id)->get());

        if($check == 0){
        $response = array(
            'status' => 'success',
            'color'  => 'red'
        );
    
        return response()->json($response);
        }
        
        else{
            $response = array(
                'status' => 'success',
                'color'  => 'blue'
            );

            return response()->json($response); 
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserTrait  $userTrait
     * @return \Illuminate\Http\Response
     */
    public function edit(UserTrait $userTrait)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserTrait  $userTrait
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserTrait $userTrait)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserTrait  $userTrait
     * @return \Illuminate\Http\Response
     */
  
}
