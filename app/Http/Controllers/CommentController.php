<?php

namespace App\Http\Controllers;
use App\User;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        // $userId = Auth::id();
        // dd($request->all());
        // $request = $request + ['user_id'=>Auth::id()];
        // dd($request->to_user_id);
        // dd($request->all());
        // $comments = Comment::create($request->all());
        $Commentfrom = Comment::where('from_user_id',Auth::id())->where('to_user_id',$request->to_user_id)->get();
       
            if(count($Commentfrom) < 5 ){
                $request["from_user_id"] = Auth::id();
                $comments = new Comment;
                $comments->body = $request->body;
                $comments->from_user_id = $request->from_user_id;
                $comments->to_user_id = $request->to_user_id;
                $comments->save();
                $numOfComments = count(Comment::where('to_user_id',$request->from_user_id)->get());
                    return back();
            }
            else{
                return "wtf are you tryna do?";
            }
        }
    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comments = Comment::where('to_user_id', $id)->get();
        $users = User::findOrFail($id);
        // dd($comments);
        return view('view_user_comments',compact ('comments','users'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
