<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Comment;
use App\UserTrait;
use App\Follows;
use Image;
use Auth;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        
        $users = User::orderBy('id', 'desc')->get();

        $comments = Comment::all();

        

    return view('users_list', compact('users', 'comments'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view('users_list');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
      //  dd($request->fcomment);

        $user = User::findOrFail($id);

        $user->update($request->all());

        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    public function overview()
    {
       $users = User::paginate(3);
       $comments= Comment::all();
         return view('show_users_overview', compact('users','comments'));
    }

    public function FollowsExists()
    {

        $check = count(Follows::where('from_user_id',auth()->id())->where('to_user_id',$id)->get());

        if($check == 0){
            return false;
            }

        else{
            return true;
            }
      
    }

    public function traitNumCounter($singleTrait)
    {
        $counter=0;
        foreach ($singleTrait as $record) {
            
            switch($record->trait){

                case "very_bad": $counter=$counter-2;
                break;
                case "bad": $counter=$counter-1;
                break;
                case "neutral":   ;
                break;
                case "good": $counter=$counter+1;
                break;
                case "very_good": $counter=$counter+2;
                break;

            }
        }
        return $counter;
    }
    public function addDescription(Request $request,$id){
        
        $user = User::findOrFail(auth()->id());
        $user->description = "$request->description";
        $user->save();

        return back();

    }
    public function percentCalc($traits)
    {
        $open = $traits->where('catagory','open')->all();
        $open_count = count($open);
        $extra = $traits->where('catagory','extra')->all();
        $extra_count = count($extra);
        $cons = $traits->where('catagory','cons')->all();
        $cons_count = count($cons);
        $agre = $traits->where('catagory','agre')->all();
        $agre_count = count($agre);
        $nero = $traits->where('catagory','nero')->all();
        $nero_count = count($nero);

        $allNumsArray = array(
            'open' => round($this->toPercent($this->traitNumCounter($open),$open_count)),
            'extra' => round($this->toPercent($this->traitNumCounter($extra),$extra_count)),
            'cons' => round($this->toPercent($this->traitNumCounter($cons),$cons_count)),
            'agre' => round($this->toPercent($this->traitNumCounter($agre),$agre_count)),
            'nero'  => round($this->toPercent($this->traitNumCounter($nero),$nero_count)),
        );

        
        return $allNumsArray;
    }

    public function toPercent($traitValue, $traitcount)
    {
        $traitcount=$traitcount;

        if($traitcount==0){
            return 0;
        }

        else{
        $endPercentage = 50 + 25 * ($traitValue/$traitcount);
        return $endPercentage;
        }

    }

    public function profile($id)
    {

        $user = User::where('id',$id)->first();
        // $array_trait = UserTrait::where('from_user_id',auth()->id())->where('to_user_id',$id)->get()->first();
        $trait = UserTrait::where('from_user_id',auth()->id())->where('to_user_id',$id)->get()->all();
        $followers_count = count(Follows::where('to_user_id',$id)->get());
        $following_count = count(Follows::where('from_user_id',$id)->get());
        $checkFollows = count(Follows::where('from_user_id',auth()->id())->where('to_user_id',$id)->get());

        if($checkFollows == 0){
            $checkFollows = "Follow";
            }
            else{
            $checkFollows = "Unfollow";
            }

        $json = json_encode($trait);
   
            return view('user',compact('user','followers_count','following_count','trait','json','checkFollows','progressPercent'));

        }
        public function uploadImage(Request $request){

            if($request->hasfile('url_img')){
                $url_img = $request->file('url_img');
                $filename = time() . '.' . $url_img->getClientOriginalExtension();
                Image::make($url_img)->fit(400,400)->save(public_path('/uploads/avatars/' . $filename));

                $user = Auth::user();
                $user->url_img = $filename;
                $user->save();
            }
            return back();
        }

}
