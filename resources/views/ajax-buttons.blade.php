<div  style="margin:2%;"><label style="margin:12px;padding:6px;">Conscientiousness</label><br>
    <div class="wrapper bag">
    <div id="open" class="btn-group" role="group">
        
        <button value="very_bad" id="A1"  class="btn new-btn btn-primary btn-trait" type="button">Very bad</button>
        <button value="bad" id="A2"  class="btn new-btn btn-primary btn-trait" type="button">Bad</button>
        <button value="neutral"  id="A3" class="btn new-btn btn-primary btn-trait " type="button">Neutral</button>
        <button  value="good"  id="A4"  class="btn new-btn btn-primary btn-trait" type="button">Good</button>
        <button  value="very_good" id="A5" class="btn new-btn btn-primary btn-trait" type="button">Very good</button>

    </div>
    <div  class="progress">
        <div class="progress-bar open"  aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%;">50%</div>
    </div>
    </div>
</div> 
          
<div  style="margin:2%;"><label style="margin:12px;padding:6px;">Conscientiousness</label><br>
    <div class="wrapper bag">
    <div id="extra" class="btn-group" role="group">

        <button value="very_bad" id="B1"  class="btn new-btn btn-primary btn-trait" type="button">Very bad</button>
        <button value="bad" id="B2"  class="btn new-btn btn-primary btn-trait" type="button">Bad</button>
        <button value="neutral"  id="B3" class="btn new-btn btn-primary btn-trait " type="button">Neutral</button>
        <button  value="good"  id="B4"  class="btn new-btn btn-primary btn-trait" type="button">Good</button>
        <button  value="very_good" id="B5" class="btn new-btn btn-primary btn-trait" type="button">Very good</button>

    </div>
    <div  class="progress">
        <div class="progress-bar extra"  aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">50%</div>
    </div>
    </div>
</div> 
<div  style="margin:2%;"><label style="margin:12px;padding:6px;">Conscientiousness</label><br>
    <div class="wrapper bag">
    <div id="cons" class="btn-group" role="group">

        <button value="very_bad" id="C1"  class="btn new-btn btn-primary btn-trait" type="button">Very bad</button>
        <button value="bad" id="C2"  class="btn new-btn btn-primary btn-trait" type="button">Bad</button>
        <button value="neutral"  id="C3" class="btn new-btn btn-primary btn-trait " type="button">Neutral</button>
        <button  value="good"  id="C4"  class="btn new-btn btn-primary btn-trait" type="button">Good</button>
        <button  value="very_good" id="C5" class="btn new-btn btn-primary btn-trait" type="button">Very good</button>

    </div>
    <div  class="progress">
        <div class="progress-bar cons"  aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">50%</div>
    </div>
    </div>
</div> 
<div  style="margin:2%;"><label style="margin:12px;padding:6px;">Conscientiousness</label><br>
    <div class="wrapper bag">
    <div id="agre" class="btn-group" role="group">

        <button value="very_bad" id="D1"  class="btn new-btn btn-primary btn-trait" type="button">Very bad</button>
        <button value="bad" id="D2"  class="btn new-btn btn-primary btn-trait" type="button">Bad</button>
        <button value="neutral"  id="D3" class="btn new-btn btn-primary btn-trait " type="button">Neutral</button>
        <button  value="good"  id="D4"  class="btn new-btn btn-primary btn-trait" type="button">Good</button>
        <button  value="very_good" id="D5" class="btn new-btn btn-primary btn-trait" type="button">Very good</button>

    </div>
    <div  class="progress">
        <div class="progress-bar agre"  aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">50%</div>
    </div>
    </div>
</div> 
<div  style="margin:2%;"><label style="margin:12px;padding:6px;">Conscientiousness</label><br>
    <div class="wrapper bag">
    <div id="nero" class="btn-group" role="group">

        <button value="very_bad" id="E1"  class="btn new-btn btn-primary btn-trait" type="button">Very bad</button>
        <button value="bad" id="E2"  class="btn new-btn btn-primary btn-trait" type="button">Bad</button>
        <button value="neutral"  id="E3" class="btn new-btn btn-primary btn-trait " type="button">Neutral</button>
        <button  value="good"  id="E4"  class="btn new-btn btn-primary btn-trait" type="button">Good</button>
        <button  value="very_good" id="E5" class="btn new-btn btn-primary btn-trait" type="button">Very good</button>

    </div>
    <div  class="progress">
        <div class="progress-bar nero"  aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">50%</div>
    </div>
    </div>
</div> 