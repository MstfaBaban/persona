@extends('layouts.app') @section('content')

<div class="container-fluid" id="rapper" style="width:96%">
<div class="row justify-content-md-center">    

@auth
    <div class="col-lg-5 buttonz">
        @include('ajax-buttons')
    </div>

    <div id="side-profile" style="float:left;" class="col-lg-7">                       
        @include('side-profile')
    </div>   

 @endauth
@guest
    <div id="side-profile" style="float:left;" class="col-lg-8">                       
        @include('side-profile')
    </div>     
@endguest
</div>

</div>


  
@include('description-edit-modal')


    
@stop

@section('custom_js')

@include('myJsFile')

@stop