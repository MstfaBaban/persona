  <!-- Edit Description Modal -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Edit Description</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        <form method="POST" action="/user/{{$user->id}}/profile/des">
            @csrf
            <div class="modal-body">
                
                    <div class="form-group">
                      <label for="message-text" class="col-form-label">Description:</label>
                      <textarea class="form-control" name="description" id="message-text">{{$user->description}}</textarea>
                      {{-- <input type="text" class="form-control" name="description"> --}}
                    </div>
                  
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" name="submit" value="submit"  class="btn new-btn">Save</button>
            </div>
        </form>
          </div>
        </div>
      </div>