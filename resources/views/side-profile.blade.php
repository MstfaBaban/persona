<div class="list-group">
<div class="img-and-info">
    <h2 id="profile-heading" >{{$user->name}}</h2>
<p id="info-with-img">{{$user->description}}</p>

@if(Auth::user()==$user)
<a id="image" href="#">
@endif
<img  id="img-with-info" class="col-10 rounded-circle" src="/uploads/avatars/{{$user->url_img}}"alt="...">
</a>

<form class="upload" id="submit" enctype="multipart/form-data" method="POST" action="/image">
@csrf
<input class="upload" id="browse" type="file" name="url_img">
<input class="upload" type="submit" class="pull-left">
</form>

</div>

<div class="col-xs-12 divider text-center">
        <div id="snip1" class="col-xs-12 col-sm-4 emphasis">
            <h2 ><strong id="followers">{{$followers_count}}</strong></h2>                    
            <p><small>Followers</small></p>
            @auth
            <button id="btn-follow" class="btn new-green-btn btn-block"><span class="fa fa-plus-circle"></span> Follow </button>
            @endauth
        </div>
        <div id="snip2" class="col-xs-12 col-sm-4 emphasis">
            <h2  ><strong id="following">{{$following_count}}</strong></h2>                    
            <p><small>Following</small></p>
            @if($user->id == auth()->id())
            <button  data-toggle="modal" data-target="#exampleModalCenter" class="btn new-blue-btn btn-block"><span class="fa fa-user"></span> Edit Description </button>
            @endif
        </div>
    
        </div>
        </div>