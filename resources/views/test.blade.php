@extends('layouts.app') @section('content')

<div class="main-wrapper-first relative">
    <header>
        <div class="container">
            <div class="header-wrap">
                <div class="header-top d-flex justify-content-between align-items-center">
                    <div class="logo">
                        <a href="index.html"><img src="img/logo.png" alt=""></a>
                    </div>
                    <div class="main-menubar d-flex align-items-center">
                        <nav class="hide">
                            <a href="index.html">Home</a>
                            <a href="generic.html">Generic</a>
                            <a href="elements.html">Elements</a>
                        </nav>
                        <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="banner-area">
        <div class="container">
            <div class="row justify-content-center height align-items-center">
                <div class="col-lg-8">
                    <div class="banner-content text-center">
                        <h1 class="text-uppercase">we’re <br> Creative</h1>
                        <a href="#" class="primary-btn d-inline-flex align-items-center"><span>Get Started</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Start Feature Area -->
    <section class="featured-area">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="single-feature d-flex flex-wrap justify-content-between">
                        <div class="icon">
                            <img src="img/f1.png" alt="">
                        </div>
                        <div class="desc">
                            <h4>Unlimited Colors</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-feature d-flex flex-wrap justify-content-between">
                        <div class="icon">
                            <img src="img/f2.png" alt="">
                        </div>
                        <div class="desc">
                            <h4>Smart Security</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-feature d-flex flex-wrap justify-content-between">
                        <div class="icon">
                            <img src="img/f3.png" alt="">
                        </div>
                        <div class="desc">
                            <h4>Endless Support</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="single-feature d-flex flex-wrap justify-content-between">
                        <div class="icon">
                            <img src="img/f4.png" alt="">
                        </div>
                        <div class="desc">
                            <h4>Smart Security</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Feature Area -->
    <!-- Start About Area -->
    <section class="about-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4">
                    <div class="about-title">
                        <h2>About <br> Our Company</h2>
                        <p>LCD screens are uniquely modern in style, and the liquid crystals that make.</p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="about-thumb">
                        <img src="img/about.jpg" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-lg-4 ml-auto">
                    <div class="tab-total-content">
                        <div class="nav ilene-tabs" id="myTab" role="tablist">
                            <a class="nav-item active" id="nav-home-tab" data-toggle="tab" href="#nav-history" role="tab" aria-controls="nav-history" aria-selected="true">History</a>
                            <a class="nav-item" id="nav-profile-tab" data-toggle="tab" href="#nav-mission" role="tab" aria-controls="nav-mission" aria-selected="false">Mission & Vision</a>
                        </div>
                        <div class="tab-content mt-40" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-history" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div class="single-content">
                                    <h3>History</h3>
                                    <p>LCD screens are uniquely modern in style, and the liquid crystals that make them work have allowed humanity to create slimmer, more portable technology.</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-mission" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <div class="single-content">
                                    <h3>Mission & Vision</h3>
                                    <p>LCD screens are uniquely modern in style, and the liquid crystals that make them work have allowed humanity to create slimmer, more portable technology.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Area -->
    <!-- Start Amazing Works Area -->
</div>



  


@stop

@section('custom_js')

@stop