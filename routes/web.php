<?php


// |--------------------------------------------------------------------------
// | Web Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register web routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | contains the "web" middleware group. Now create something great!
// |


Route::get(

    '/ff', function () {




        return view('welcome');

    }
);

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('login/twitter', 'Auth\LoginController@redirectToProvider');

Route::get('login/twitter/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('users/overview','UsersController@overview');

Route::resource('user', 'UsersController');

Route::get('user/{id}/profile','UsersController@profile');

Route::post('/postajax','HomeController@ajax');

Route::put('user/{id}/addTrait','UsersController@addTrait');

Route::resource('comment','CommentController');

Route::resource('trait','UserTraitController');

//Route::resource('follows','FollowsController');

Route::post('trait/{id}','UserTraitController@store');

Route::post('user/{id}/profile/des','UsersController@addDescription');

Route::post('image','UsersController@uploadImage');

Route::get('trait/{id}','UserTraitController@check');

Route::get('test',function(){
    return view('test');
});

Route::post('follows/{id}','FollowsController@store');

Route::get('follows/{id}','FollowsController@show');

Route::get(

    '/logout', function () {

        Auth::logout();

        return Redirect::to('/login');

    }
);
